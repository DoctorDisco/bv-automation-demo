from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from pages.PageObject import PageObject


class LaunchPage(PageObject):

    class Locators(object):
        LAUNCH_STATISTIC1 = (By.ID, 'stat-statistic-1')

    def wait_for_it(self):
        WebDriverWait(self.webdriver, 5).until(ec.presence_of_element_located(self.Locators.LAUNCH_STATISTIC1))
