import yaml
import os


class ConfigLoader(object):

    def __init__(self, path):
        self.config = None
        self.reason = None
        self.path = path

    def load_config(self):
        if not self.config:
            with open(self.path, 'r') as f:
                self.config = yaml.load(f)
                if not self.validate_config(self.config):
                    self.config = None
                    self.reason = "Configuration did not pass validation"
                    print("Configuration loading failed because {}".format(self.reason))
        else:
            print('Config already loaded!')

    def validate_config(self, config):
        # TODO: validate config
        return True

    def get_config(self):
        if self.config is None:
            self.load_config()
        return self.config

    @staticmethod
    def get_user_creds():
        return os.environ['TEST_USER'], os.environ['TEST_PASSWORD']

    @staticmethod
    def get_slack_webhook():
        return os.environ['WEBHOOK_URL']



