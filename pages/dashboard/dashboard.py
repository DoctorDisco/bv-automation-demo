import selenium.webdriver.support.expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from pages.PageObject import PageObject
from pages.dashboard.top_bar import TopBarComponent


class DashboardPage(PageObject):

    class Locators(object):
        """ A class containing tuples used for locating elements in the application """
        LAUNCH_BUTTON = (By.CLASS_NAME, 'tb-rocket')
        # EXPECTED_PAGE_TITLE = "Expense - Dashboard"
        TOOLBAR_CLASS = (By.CLASS_NAME, 'd-head-toolbar')

    def wait_for_it(self):
        """ Wait for the dashboard page to load """
        WebDriverWait(self.webdriver, 15)\
            .until(ec.visibility_of_element_located(self.Locators.TOOLBAR_CLASS))

    def get_top_bar(self):
        """ Return TopBarComponent to interact with the nav bar at the top of the page """
        return TopBarComponent(self)



