#!/bin/bash

echo "Sourcing environment.sh"
source environment.sh
if [ $? -ne 0 ]; then
    echo "Could not source the environment file. Check permissions or create the file and try again."
    exit 1
fi

if [ -z "$TEST_USER" ]; then
    echo "Need to set TEST_USER"
    exit 1
else
    echo "TEST_USER is $TEST_USER"
fi

if [ -z "$TEST_PASSWORD" ]; then
    echo "Need to set TEST_PASSWORD"
    exit 1
fi

function deactivate_virtualenv() {
    echo "deactivating virtual environment"
    deactivate
}

# Check if virtual environment exists. If not, create it.
echo "Checking if virtualenv exists for running tests."
if [ ! -d env/ ]; then
    echo "Virtualenv does not exist. Creating it."
    virtualenv -p python3.6 env
    if [ $? -ne 0 ]; then
        echo "Could not create virtualenv, quitting."
        deactivate_virtualenv
        exit 1
    fi
fi

# Activate virtual environment
echo "Activating virtual environment."
source env/bin/activate


# Install requirements
echo "Installing requirements."
pip install -r requirements.txt
if [ $? -ne 0 ]; then
    echo "Requirement installation failed."
    deactivate_virtualenv
    exit 1
fi

# Run the tests.
echo "Running tests..."
python main.py
if [ $? -ne 0 ]; then
    echo "Something went wrong. Review the test output, make adjustments, and try again."
    deactivate_virtualenv
    exit 1
fi

echo "Everything is fine."
exit 0
