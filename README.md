# bv-automation-demo

A simple demonstration of BV1.5 instance automation using Python w/ Selenium.

## Reading Material

If you are unfamiliar with Selenium or Python, you will probably want to seek out some introductory learning materials on both subjects. 
For learning Python starting from scratch try [Learn Python the Hard Way](https://learnpythonthehardway.org/book/)
Excellent documentation about how to use Selenium with Python can be found on [readthedocs.org](http://selenium-python.readthedocs.io/).

## Pre-reqs

To get things working, there are a few things you'll need installed in the environment where tests should run. Most of these things are common items associated with a normal Python environment.

* Python, specifically [Python 3.6](https://www.python.org/downloads/release/python-360/)
* pip (in most cases this is bundled with Python)
* virtualenv (Globally `pip3 install virtualenv`)

To use these tests you'll need to install the appropriate driver for the desired browser
- For Firefox:
    * On Mac `brew install geckodriver`. If you don't have `brew` installed, you probably should.
    * On Windows, [download and install the driver](https://github.com/mozilla/geckodriver/releases)
- For IE:
    * 🔥 Locate your nearest emergency exit 🔥
    * Find and install an appropriate driver [such as InternetExplorerDriver](https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver)
- For Chrome
    * On Mac `brew install chromedriver`
    * On Windows [download and install the driver](https://sites.google.com/a/chromium.org/chromedriver/downloads)

## Setup steps for running tests

Prior to running the tests, check the configuration in config.yaml. Only non-sensitive configuration parameters should go in config.yaml.

For sensitive configuration parameters such as passwords, user names, webhooks, and other values that shouldn't be shared with the world at large, these should be set as environment variables. The required variables are defined below.

* TEST_USER - The user logging in to the application.
* TEST_PASSWORD - The password for the user logging in to the application.
* SLACK_WEBHOOK - The webhook URL for posting test notifications to slack

## Running the Tests

You'll need to set up your virtual environment, activate it, install required packages, then run main.py.

    # virtualenv -p python3.6 env
    # source env/bin/activate
    # pip install -r requirements.txt
    # python main.py

OR Just run the example bash script (see example below). Make sure you have created an environment.sh file with the required environment variables. Here's a sample
environment file:

    #!/usr/bin/env bash
    export TEST_USER="user_goes_here"
    export TEST_PASSWORD="password_goes_here"

## Example Output

    $ ./test.sh
    Sourcing environment.sh
    TEST_USER is someuser@somehost.com
    Checking if virtualenv exists for running tests.
    Activating virtual environment.
    Installing requirements.
    Requirement already satisfied: selenium in ./env/lib/python3.6/site-packages (from -r requirements.txt (line 1))
    Requirement already satisfied: requests in ./env/lib/python3.6/site-packages (from -r requirements.txt (line 2))
    Requirement already satisfied: pyyaml in ./env/lib/python3.6/site-packages (from -r requirements.txt (line 3))
    Requirement already satisfied: faker in ./env/lib/python3.6/site-packages (from -r requirements.txt (line 4))
    Requirement already satisfied: six in ./env/lib/python3.6/site-packages (from faker->-r requirements.txt (line 4))
    Requirement already satisfied: python-dateutil>=2.4 in ./env/lib/python3.6/site-packages (from faker->-r requirements.txt (line 4))
    Running tests...
    Running automated testing demo
    .ss.......
    ----------------------------------------------------------------------
    Ran 10 tests in 44.749s

    OK (skipped=2)
    Everything is fine.


## Code organization

These tests use the Page Object Pattern (POP). Individual page objects are lumped into the `pages` package. 
The pages package is then organized into subpackages by area of the application.

The `tests` package contains the source code for the various tests. Each test class directly extends `unittest.Testcase`.






