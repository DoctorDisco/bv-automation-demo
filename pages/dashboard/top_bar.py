from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from pages.PageComponent import PageComponent
from pages.dashboard.daily import DailyPage
from pages.dashboard.launch import LaunchPage
from pages.dashboard.planning import PlanningPage
from pages.dashboard.reporting import ReportingPage
from pages.expense.expense import ExpensePage


class TopBarComponent(PageComponent):

    class Locators(object):
        LAUNCH_BUTTON = (By.CLASS_NAME, 'tb-rocket')
        REPORTING_BUTTON = (By.CLASS_NAME, 'tb-reporting')
        DAILY_BUTTON = (By.CLASS_NAME, 'tb-daily')
        PLANNING_BUTTON = (By.CLASS_NAME, 'tb-planning')
        EXPENSE_BUTTON = (By.CLASS_NAME, 'tb-expense')

    def click_launch_button(self):
        self.parent_page.webdriver.find_element(*self.Locators.LAUNCH_BUTTON).click()
        return LaunchPage(self.parent_page.webdriver)

    def click_reporting_button(self):
        self.parent_page.webdriver.find_element(*self.Locators.REPORTING_BUTTON).click()
        return ReportingPage(self.parent_page.webdriver)

    def click_daily_button(self):
        self.parent_page.webdriver.find_element(*self.Locators.DAILY_BUTTON).click()
        return DailyPage(self.parent_page.webdriver)

    def click_planning_button(self):
        self.parent_page.webdriver.find_element(*self.Locators.PLANNING_BUTTON).click()
        return PlanningPage(self.parent_page.webdriver)

    def click_expense_button(self):
        WebDriverWait(self.parent_page.webdriver, 5).until(ec.element_located_selection_state_to_be(self.Locators.EXPENSE_BUTTON, is_selected=False))
        expense_button = self.parent_page.webdriver.find_element(*self.Locators.EXPENSE_BUTTON)
        assert (expense_button is not None)

        expense_button.click()

        return ExpensePage(self.parent_page.webdriver)
