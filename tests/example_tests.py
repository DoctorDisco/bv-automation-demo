import unittest

from selenium import webdriver

from configloader import ConfigLoader
from pages.login_page import LoginPage


class LoginPageTests(unittest.TestCase):

    def setUp(self):
        self.config = ConfigLoader('config.yaml')
        self.webdriver = webdriver.Firefox()
        self.webdriver.get(self.config.get_config()['demo_url'])
        self.test_user, self.test_password = self.config.get_user_creds()

    def tearDown(self):
        self.webdriver.quit()

    def test_login_page(self):
        login_page = LoginPage(self.webdriver)
        login_page.set_user(self.test_user)
        login_page.set_password(self.test_password)
        login_page.log_in()

    def test_no_password(self):
        login_page = LoginPage(self.webdriver)
        login_page.set_user(self.test_user)
        login_page.click_login_button()
        self.assertTrue(login_page.has_error('Please enter a password'), 'Wrong error message displayed')

    def test_no_user(self):
        login_page = LoginPage(self.webdriver)
        login_page.set_password(self.test_password)
        login_page.click_login_button()
        self.assertTrue(login_page.has_error('Please enter a user name'), 'Wrong error message displayed')

    def test_no_user_or_password(self):
        login_page = LoginPage(self.webdriver)
        login_page.click_login_button()
        self.assertTrue(login_page.has_error('Please enter a user name'), 'Wrong error message displayed')


class DashboardTests(unittest.TestCase):

    def setUp(self):
        self.config = ConfigLoader('config.yaml')
        self.webdriver = webdriver.Firefox()
        self.webdriver.get(self.config.get_config()['demo_url'])
        login_page = LoginPage(self.webdriver)
        self.test_user, self.test_password = self.config.get_user_creds()
        login_page.set_user(self.test_user)
        login_page.set_password(self.test_password)
        self.dashboard_page = login_page.log_in()

    def test_launch_page_load(self):
        launch_page = self.dashboard_page.get_top_bar().click_launch_button()
        launch_page.wait_for_it()

    @unittest.skip('TODO')
    def test_reporting_page_load(self):
        # reporting_page = self.dashboard_page.get_top_bar().click_report_button()
        # reporting_page.wait_for_it()
        pass

    @unittest.skip('TODO')
    def test_logout(self):
        pass

    def tearDown(self):
        self.webdriver.quit()


class ExpenseTests(unittest.TestCase):

    def setUp(self):
        self.config = ConfigLoader('config.yaml')
        self.webdriver = webdriver.Firefox()
        self.webdriver.get(self.config.get_config()['demo_url'])
        login_page = LoginPage(self.webdriver)
        self.test_user, self.test_password = self.config.get_user_creds()
        login_page.set_user(self.test_user)
        login_page.set_password(self.test_password)
        self.dashboard_page = login_page.log_in()
        self.dashboard_page.wait_for_it()
        self.expense_page = self.dashboard_page.get_top_bar().click_expense_button()

    def tearDown(self):
        self.webdriver.quit()

    def test_expense_page_loads(self):
        self.expense_page.wait_for_it()

    def test_expense_search(self):
        self.expense_page.wait_for_it()
        search_page = self.expense_page.get_search_panel()
        search_page.choose_vendor('Vendor 0099930 (0099930)')
        search_page.click_search()

    def test_tab_load(self):
        self.expense_page.wait_for_it()
        # TODO - Figure out why this sleep was needed to make it work. May need to revise expense_page.wait_for_it()
        from time import sleep
        sleep(5)
        self.expense_page.click_posted_tab()
        self.expense_page.click_approved_tab()
        self.expense_page.click_submitted_tab()
        self.expense_page.click_unsubmitted_tab()
        self.expense_page.click_scanned_files_tab()





