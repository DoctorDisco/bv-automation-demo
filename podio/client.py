import requests
import json

from configloader import ConfigLoader


class PodioClient(object):
    """ Simple Podio client that uses the easiest authentication to interact with apps"""

    def __init__(self, client_token, client_secret, app_id, app_token):
        self.client_token = client_token
        self.client_secret = client_secret
        self.app_id = None
        self.oauth_token = None
        self.refresh_token = None
        self.app_id = app_id
        self.app_token = app_token
        self.oauth_token, self.refresh_token = self._oauth_token()

    def _oauth_token(self):
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        body = "grant_type=app&app_id={}&app_token={}&client_id={}&redirect_uri=someplace&client_secret={}"\
            .format(self.app_id, self.app_token, self.client_token, self.client_secret)

        if self.oauth_token is None:
            response = requests.post("https://podio.com/oauth/token", data=body, headers=headers)
            if response.status_code != 200:
                print("Unable to obtain authentication token, terminating all the things.")
                print("Status code was {} and message was {}".format(response.status_code, str(response.content)))
                print("original request was {}".format(str(response.request)))
                exit(1)
        response_body = json.loads(response.content)
        oauth_token = response_body['access_token']
        refresh_token = response_body['refresh_token']
        return oauth_token, refresh_token

    def report_test_stats(self, test_passed, tests_failed, tests_errored, duration, environment=None, failed_cases=None):
        """ Creates a new Podio item in the 'Automated Test Runs' app """
        headers = {
            'Content-type': 'application/json',
            'Authorization': 'OAuth2 {}'.format(self.oauth_token)
        }

        body = {
            "title": "Test run ",
            "date": {
                'start': "2011-12-31 11:27:10",
                'end': "2011-12-31 11:27:10"
            },
            'category': 1,
            'tests-passed': test_passed,
            'tests-failed': tests_failed,
            'tests-in-error': tests_errored,
            'total-execution-time': duration,
        }

        print(json.dumps(headers))
        print(json.dumps(body))
        response = requests.post(url="https://api.podio.com/item/app/{}".format(self.app_id), json=json.dumps(body),
                                 headers=headers)
        if response.status_code != 200:
            print('unable to create new item')
            print('status code was {}'.format(response.status_code))
            print('message was {}'.format(json.loads(response.content)))
        pass

if __name__ == '__main__':
    config = ConfigLoader("config.yaml")
    client_token = config.get_config()['client_id']
    client_secret = config.get_config()['client_secret']
    app_id = config.get_config()['app_id']
    app_token = config.get_config()['app_token']
    client = PodioClient(client_token, client_secret, app_id, app_token)
    client.report_test_stats(123, 123, 123, 225)
