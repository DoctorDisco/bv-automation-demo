from selenium.webdriver.common.by import By

from pages.PageObject import PageObject
from pages.dashboard.dashboard import DashboardPage


class LoginPage(PageObject):

    class Locators(object):
        """ A collection of tuples used to locate UI elements. """
        NAV_PENDING = (By.XPATH, ".//*[@id='menu-invoice-pending']")
        LOGIN_BUTTON = (By.ID, 'loginButton')
        USER_TEXT = (By.ID, 'userBox')
        PASSWORD_TEXT = (By.ID, 'passBox')
        STATUS_LABEL = (By.ID, 'statusLabel')

    def is_login_page(self):
        return self.webdriver.title == 'PortfolioOne™ Login'

    def log_in(self):
        """ Clicks the login button and waits for arrival at the dashboard page. """
        self.click_login_button()
        dashboard_page = DashboardPage(self.webdriver)
        dashboard_page.wait_for_it()
        return dashboard_page

    def click_login_button(self):
        self.webdriver.find_element(*self.Locators.LOGIN_BUTTON).click()

    def set_user(self, user):
        self.webdriver.find_element(*self.Locators.USER_TEXT).send_keys(user)

    def set_password(self, password):
        self.webdriver.find_element(*self.Locators.PASSWORD_TEXT).send_keys(password)

    def reset_password(self):
        self.webdriver.find_element()

    def has_error(self, message):
        return str(self.webdriver.find_element(*self.Locators.STATUS_LABEL).text).strip() == message


