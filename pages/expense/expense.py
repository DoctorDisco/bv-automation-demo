from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from pages.PageComponent import PageComponent
from pages.PageObject import PageObject
from time import sleep


class SearchPanelComponent(PageComponent):
    """ Page component representing the search panel from the Expense page """

    # Element locators
    VENDOR_DROPDOWN = (By.ID, 'ddlVendor-ddb')
    VENDOR_DROPDOWN_ARROW = (By.CLASS_NAME, 'ui-menu-item')
    ACCOUNT_DROPDOWN = (By.ID, 'ddlAccount-cbb')
    SEARCH_PANEL = (By.ID, 'searchPanel')

    def choose_property(self, property_name):
        """ Chooses a property in the search panel
         :param property_name a string representing the property to be selected """
        # TODO
        pass

    def click_search(self):
        """ Clicks the Search button to initiate the search """
        # TODO
        pass

    def click_clear(self):
        """ Clicks the Clear button to clear the search criteria"""
        # TODO
        pass

    def choose_vendor(self, vendor):
        """ Chooses a vendor from the dropdown dialog in the search panel.
        :param vendor is expected to be a string matching one of the vendors in the drop-down"""

        self._open_vendor_dropdown()
        WebDriverWait(self.parent_page.webdriver, 10) \
            .until(ec.presence_of_all_elements_located(self.VENDOR_DROPDOWN_ARROW))
        menu_items = self.parent_page.webdriver.find_elements(*self.VENDOR_DROPDOWN_ARROW)
        for menu_item in menu_items:
            if menu_item.text == vendor:
                menu_item.click()
                return menu_item
        return None

    def choose_gl_account(self, gl_account):
        """ Choose an account from the search panel's dropdown control """
        # TODO
        pass

    def _open_vendor_dropdown(self):
        sleep(3)
        vendor_drop_down = self.parent_page.webdriver.find_element(*self.VENDOR_DROPDOWN)
        vendor_drop_down.find_element(By.CLASS_NAME, 'ui-button-icon-primary').click()

    def _get_searchpanel_element(self):
        self.parent_page.webdriver.find_element(*self.SEARCH_PANEL)


class ExpensePage(PageObject):
    """ PageObject representing the Expense page of the application. """

    # Element locators
    SEARCH_PANEL = (By.ID, 'searchPanel')

    EXPENSE_IFRAME_NAME = 'expenseiframe'
    EXPENSE_IFRAME = (By.ID, EXPENSE_IFRAME_NAME)

    SCANNED_FILES_TAB = (By.ID, 'lnkScans')
    SCANNED_FILES_LOADED = (By.ID, 'gridAttach')

    UNSUBMITTED_TAB = (By.ID,  'lnkInvoicesSaved')
    UNSUBMITTED_LOADED = (By.ID, 'gridInvStyleSheetHolder')

    SUBMITTED_TAB = (By.ID, 'lnkInvoicesSubmitted')
    SUBMITTED_LOADED = (By.ID, 'gridSubmittedStyleSheetHolder')

    APPROVED_TAB = (By.ID, 'lnkInvoicesApproved')
    APPROVED_LOADED = (By.ID, 'gridApprovedStyleSheetHolder')

    POSTED_TAB = (By.ID, 'lnkInvoicesPosted')
    POSTED_LOADED = (By.ID, 'gridPostedStyleSheetHolder')

    def wait_for_it(self):
        """ Waits for the Expense page to finish loading """
        WebDriverWait(self.webdriver, 30)\
            .until(ec.visibility_of_element_located(self.EXPENSE_IFRAME), "could not find iframe for expense page")

    def get_search_panel(self):
        """ Returns a SearchPanel object that can be used to interact with the Expense page's search panel """
        self.webdriver.switch_to.frame(self.EXPENSE_IFRAME_NAME)
        return SearchPanelComponent(self)

    def click_scanned_files_tab(self):
        """ Click on the Scanned Files tab and wait for it to load """
        self.webdriver.switch_to.frame(self.EXPENSE_IFRAME_NAME)
        self.webdriver.find_element(*self.SCANNED_FILES_TAB).click()
        WebDriverWait(self.webdriver, 10).until(ec.presence_of_element_located(self.SCANNED_FILES_LOADED))
        self.webdriver.switch_to.default_content()

    def click_unsubmitted_tab(self):
        """ Click on the Unsubmitted tab and wait for it to load """
        self.webdriver.switch_to.frame(self.EXPENSE_IFRAME_NAME)
        self.webdriver.find_element(*self.UNSUBMITTED_TAB).click()
        WebDriverWait(self.webdriver, 10).until(ec.presence_of_element_located(self.UNSUBMITTED_LOADED))
        self.webdriver.switch_to.default_content()

    def click_submitted_tab(self):
        """ Click on the Submitted tab and wait for it to load """
        self.webdriver.switch_to.frame(self.EXPENSE_IFRAME_NAME)
        self.webdriver.find_element(*self.SUBMITTED_TAB).click()
        WebDriverWait(self.webdriver, 10).until(ec.presence_of_element_located(self.SUBMITTED_LOADED))
        self.webdriver.switch_to.default_content()

    def click_approved_tab(self):
        """ Click on the Approved tab and wait for it to load """
        self.webdriver.switch_to.frame(self.EXPENSE_IFRAME_NAME)
        self.webdriver.find_element(*self.APPROVED_TAB).click()
        WebDriverWait(self.webdriver, 10).until(ec.presence_of_element_located(self.APPROVED_LOADED))
        self.webdriver.switch_to.default_content()

    def click_posted_tab(self):
        """ Click on the Posted tab and wait for it to load """
        self.webdriver.switch_to.frame(self.EXPENSE_IFRAME_NAME)
        self.webdriver.find_element(*self.POSTED_TAB).click()
        WebDriverWait(self.webdriver, 10).until(ec.presence_of_element_located(self.POSTED_LOADED))
        self.webdriver.switch_to.default_content()

